package com.example.countdownapp.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.countdownapp.MainActivity
import com.example.countdownapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    //creazione di una variabile per l'autenticazione
    private lateinit var auth: FirebaseAuth
    private val TAG="LoginActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // inizializzazione della variabile
        auth =Firebase.auth

        //listener per il loginButton e il registerTextView
        loginButton.setOnClickListener(){
            onLoginClick()
        }
        registerTextView.setOnClickListener(){
            onSignUpClick()
        }
    }

    private fun onLoginClick(){
        //accquisizione informazioni input
        val email: String= emailEditText.text.toString().trim()
        val password: String= passwordEditText.text.toString().trim()

        //controllo inserimento dati
        if(email.isEmpty()){
            emailEditText.error="inserire un indirizzo e-mail"//messaggio di errore sul campo
            return
        }
        if(password.isEmpty()){
            passwordEditText.error="inserire una password"
        }

        //funzione di login

        loginUser(email, password)
    }

    private fun loginUser(email:String,password:String){
        Log.d(TAG, "signInWithEmail:prova")
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d(TAG, "signInWithEmail:success")
                    //faccio partire la mainActivity e chiuso questa activity
                    val intent= Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()

                }


            }
    }

    private fun onSignUpClick(){
        // demanda il compito ad un'altra activity
        val intent= Intent(this, SignUpActivity::class.java)
        startActivity(intent)
        finish()
    }
}
