package com.example.countdownapp

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.ui.AppBarConfiguration
import kotlinx.android.synthetic.main.activity_add_exam.*
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class AddExamActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private val TAG="AddExamActivity"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_exam)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)


        dateEditText.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                dateEditText.text = (" " + dayOfMonth + "/" + (monthOfYear+1) + "/" + year)
            }, year, month, day)

            dpd.show()
        }

        timeEditText.setOnClickListener{
            val c = Calendar.getInstance()
            val hour= c.get(Calendar.HOUR_OF_DAY)
            val min= c.get(Calendar.MINUTE)
            val tpd= TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                //display selected time in textbox
                timeEditText.text=String.format("%02d:%02d", hourOfDay, minute)
            },hour,min, true)

            tpd.show()

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.getItemId()
        if (id == R.id.check) {// controllo superfluo predisposto per eventuali altre opzioni
            val resultIntent = Intent()
            val exam = examEditText.text.toString().trim()
            val date = dateEditText.text.toString().trim()
            val time = timeEditText.text.toString().trim()


            resultIntent.putExtra("EXAM_SUBJECT", subjectEditText.text.toString())
            val regex = "[\\p{Punct}]"
            //Compiling the regular expression
            //Compiling the regular expression
            val pattern = Pattern.compile(regex)
            //Retrieving the matcher object
            //Retrieving the matcher object
            val matcher: Matcher = pattern.matcher(exam)

            if (exam.isEmpty()) {
                examEditText.error = "Insert exam name"
                return false
            } else if (matcher.find()) {
                examEditText.error = "Punctuation not Allowed"
                return false

            } else resultIntent.putExtra("EXAM_EXAM", exam)

            resultIntent.putExtra("EXAM_TYPOLOGY", typologyEditText.text.toString())

            if (date.isEmpty()) {
                dateEditText.error = "insert valid date"
                return false
            } else resultIntent.putExtra("EXAM_DATE", date)

            if(time.isEmpty()){
                timeEditText.error="insert valid time"
                return false
            } else resultIntent.putExtra("EXAM_TIME", time)

            Log.d(TAG, "onOptionsItemSelected() -> " + examEditText.text)
            setResult(Activity.RESULT_OK, resultIntent)
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_add, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        setResult(Activity.RESULT_CANCELED)
       finish()
        return true
    }


}
