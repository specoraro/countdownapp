package com.example.countdownapp

import com.google.firebase.database.Exclude
import java.text.SimpleDateFormat
import java.util.*

data class Exam(var ex: String = "",
                var sub: String ="",
                var typ: String ="",
                var dateAndHour: String ="",
                @get:Exclude var date:GregorianCalendar=GregorianCalendar()) {

    constructor (exam:String) : this() {
        this.ex=exam
    }


    fun dateToString(date: GregorianCalendar): String {
        return SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ITALIAN).format(date.getTime())
    }

    override fun toString(): String {

        return "$dateAndHour:\n >>" +ex
    }







}