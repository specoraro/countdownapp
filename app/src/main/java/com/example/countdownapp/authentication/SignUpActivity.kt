package com.example.countdownapp.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.countdownapp.MainActivity
import com.example.countdownapp.R
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : AppCompatActivity() {

    //creazione di una variabile per l'autenticazione
    private lateinit var auth: FirebaseAuth
    private val TAG="SignUpActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        // inizializzazione della variabile
        auth=Firebase.auth

        //settaggio dei listener per le scelte
        registerButton.setOnClickListener(){
            onSignUpClick()
        }
        loginTextView.setOnClickListener(){
            val intent= Intent(this, LoginActivity::class.java)// ritorno alla schermata di login
            startActivity(intent)
            finish()
        }
    }

    private fun onSignUpClick(){
        val username=usernameEditText.text.toString().trim()
        val email =emailEditText.text.toString().trim()
        val password =passwordEditText.text.toString().trim()

        //controllo inserimento dati
        if (username.isEmpty()){
            usernameEditText.error="inserire uno username"
        }
        if(email.isEmpty()){
            emailEditText.error="inserire un indirizzo e-mail"//messaggio di errore sul campo
            return
        }
        if(password.isEmpty()){
            passwordEditText.error="inserire una password"
        }
        if (password.length <=6){
            passwordEditText.error="too short"
        }

        signUpUser(username, email, password)
    }

    private fun signUpUser(username: String, email: String, password: String){

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val currentUser = auth.currentUser
                    val uid=currentUser!!.uid
                    //creazione di una hashmap per i dati
                    val userMap= HashMap<String, String>()

                    userMap["name"]=username // inserisco lo username nel campo name associato alla nostra istanza
                    val dbUsers = FirebaseDatabase.getInstance().getReference("Users").child(uid)// riferimento al figlio del nodo users associato al nostro uid(utente corrente)
                    dbUsers.setValue(userMap).addOnCompleteListener(){
                            task: Task<Void> ->
                        if(task.isSuccessful){
                            Toast.makeText(baseContext, "Registration succeded.",
                                Toast.LENGTH_SHORT).show()
                            val intent= Intent(this, MainActivity::class.java)
                            startActivity(intent)
                            finish()

                        }
                    }


                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Registration failed.",
                        Toast.LENGTH_SHORT).show()

                }

            }
    }
}
