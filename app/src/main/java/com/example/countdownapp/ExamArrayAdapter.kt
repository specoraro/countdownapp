package com.example.countdownapp

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ExamArrayAdapter(private var activity: Activity, private var exams: ArrayList<Exam>) :  BaseAdapter(){


    private class ViewHolder(row: View?) {
        var subjectTextView: TextView? = null
        var examTextView:TextView?=null
        var typologyTextView:TextView?=null
        var countTextView: TextView?=null
        var dateTextView: TextView?=null


        init {
            this.subjectTextView = row?.findViewById(R.id.subjectTextView)
            this.examTextView = row?.findViewById(R.id.examTextView)
            this.typologyTextView = row?.findViewById(R.id.typologyTextView)
            this.countTextView=row?.findViewById(R.id.countTextView)
            this.dateTextView=row?.findViewById(R.id.dateTextView)

        }
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        //riciclo delle viste
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item, null)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        var examItem = exams[position]
        viewHolder.subjectTextView?.text=examItem.sub
        viewHolder.examTextView?.text=examItem.ex
        viewHolder.typologyTextView?.text=examItem.typ
        viewHolder.dateTextView?.text=examItem.dateAndHour

        val df: DateFormat = SimpleDateFormat("dd/MM/yy HH:mm")
        var date: Date = df.parse(examItem.dateAndHour)
        val cal = GregorianCalendar()
        cal.setTime(date)
        val handler=Handler()
        updateTime(viewHolder, cal,handler)
        handler.post(object : Runnable {
            override fun run() {
                handler.postDelayed(this, 1000)
                updateTime(viewHolder, cal, handler)
            }
        })



        return view
    }

    override fun getItem(position: Int): Any {
        return exams[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return exams.size
    }

    private fun updateTime(viewHolder: ViewHolder, calendar: Calendar, handler: Handler){
        //current date
        val currentDate:Calendar= Calendar.getInstance()
        //event date
        val eventDate=calendar
        /*eventDate[Calendar.YEAR] = 2021
        eventDate[Calendar.MONTH] = 0 // 0-11 so 1 less
        eventDate[Calendar.DAY_OF_MONTH] = 1
        eventDate[Calendar.HOUR] = 0
        eventDate[Calendar.MINUTE] = 0
        eventDate[Calendar.SECOND] = 0
        eventDate.timeZone = TimeZone.getTimeZone("GMT")*/

        val deltaTime= eventDate.timeInMillis-currentDate.timeInMillis
        // Change the milliseconds to days, hours, minutes and seconds
        val days = deltaTime / (24 * 60 * 60 * 1000)
        val hours = deltaTime / (1000 * 60 * 60) % 24
        val minutes = deltaTime / (1000 * 60) % 60
        val seconds = (deltaTime / 1000) % 60

        viewHolder.countTextView?.text="${days}d\n${hours}h\n${minutes}m\n${seconds}s"

        endEvent(viewHolder,currentDate, eventDate, handler)


    }

    private fun endEvent(viewHolder:ViewHolder, currentdate: Calendar, eventdate: Calendar, handler: Handler) {
        if (currentdate.time >= eventdate.time) {
            viewHolder.countTextView?.text = "GOOD LUCK!"
            //Stop Handler
            handler.removeMessages(0)
        }
    }

}