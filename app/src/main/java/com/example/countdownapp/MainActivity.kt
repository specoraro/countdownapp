package com.example.countdownapp

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.ui.AppBarConfiguration
import com.example.countdownapp.R.*
import com.example.countdownapp.authentication.LoginActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.text.DateFormat
import java.text.SimpleDateFormat
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private val TAG="MainActivity"
    val NEW_ITEM_REQUEST = 1

    private lateinit var appBarConfiguration: AppBarConfiguration
    //reference al RealtimeDB
    private lateinit var database: DatabaseReference

    var adapter: ExamArrayAdapter by Delegates.notNull()
    var exams=ArrayList<Exam>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        Log.w(TAG, "INIZIO")

        var lv: ListView? = null
        var searchView: SearchView? = null

        //set della list e dell'adapter
        lv =findViewById<View>(id.list_view) as ListView
        adapter = ExamArrayAdapter(this, exams)
        lv!!.adapter = adapter

        // inizializzazione della variabile
        auth = Firebase.auth
        var user:FirebaseUser?=auth.currentUser

        //INIZIALIZZAZIONE DELLA TOOLBAR
        val toolbar: Toolbar = findViewById(id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(id.drawer_layout)
        val nav:NavigationView=findViewById(R.id.nav_view)

        val emailTextView:TextView=findViewById(R.id.emailTexView)
        val email=auth.currentUser?.email
        emailTextView.text=email

        //listener per il log out
        val imageView: ImageView=findViewById(R.id.imageView)
        imageView.setOnClickListener(){
            var user:FirebaseUser?=auth.currentUser
            Log.d(TAG, "action SignOut clicked")
            FirebaseAuth.getInstance().signOut()
            user = null
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
        }
        //listener per il addButton
        val add: FloatingActionButton = findViewById(id.addButton)
        add.setOnClickListener {
            val intent= Intent(applicationContext, AddExamActivity::class.java)
            startActivityForResult(intent, NEW_ITEM_REQUEST)
        }

        //settaggio del listener per eliminare un elemento
        lv.setOnItemLongClickListener { parent, view, position, id ->
            onLongClickRemoveItem(position, lv)
        }
        //settaggio listener visualizzazione dei dettagli
        lv.setOnItemClickListener { parent, view, position, id ->
            OnCLickSpecs(position, lv)
        }
    }

    // per il controllo del login
    public override fun onStart(){
        super.onStart()
        val currentUser= auth.currentUser
        if( currentUser==null){
            val intent= Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
        //caricamento dal Realtime DB
        val email=auth.currentUser!!.email
        val uid= auth.currentUser!!.uid
        Log.d(TAG, "onStart() --> $email")
        database = Firebase.database.reference.child("Users").child(uid).child("EXAMS")

        val examListener=object :ValueEventListener{
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                exams.clear()
                for(datasnap:DataSnapshot in dataSnapshot.children) {
                    val exam = datasnap.getValue(Exam::class.java)
                    exams.add(exam!!)

                }
                adapter!!.notifyDataSetChanged()

            }
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

        }
        database.addValueEventListener(examListener)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Check which request we're responding to
        if (requestCode == NEW_ITEM_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                val returnValue = data?.getStringExtra("EXAM_EXAM")
                val deadline = data?.getStringExtra("EXAM_DATE")
                val hour=data?.getStringExtra("EXAM_TIME")
                val subject=data?.getStringExtra("EXAM_SUBJECT")
                val typology=data?.getStringExtra("EXAM_TYPOLOGY")
                val df: DateFormat = SimpleDateFormat("dd/MM/yy HH:mm")
                val dateAndHour="$deadline $hour"

                Log.d("TAG", "onActivityResult: --> $dateAndHour")// da modificare in return value
                if(returnValue!=null){
                    addNewItem(returnValue,dateAndHour,subject, typology)
                }
            }
        }
    }

    /*override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.getItemId()
        var user:FirebaseUser?=auth.currentUser
        if(id==R.id.logout){
            Log.d(TAG, "action SignOut clicked")
            FirebaseAuth.getInstance().signOut()
            user = null
            startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            finish()
            return true

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }*/

    fun addNewItem(exam:String,dateAndHour: String, subject:String?, typology:String?){
        if (exam.isEmpty()) {
            Toast.makeText(
                applicationContext,
                "Empty Exam string",
                Toast.LENGTH_LONG
            ).show()
            return
        }
        val newExam=Exam(exam)
        newExam.dateAndHour=dateAndHour

        if (subject != null) {
            newExam.sub=subject!!
        }

        if (typology != null) {
            newExam.typ=typology!!
        }
        //inserimento nel db Realtime Firebase
        val uid= auth.currentUser!!.uid
        database = Firebase.database.reference.child("Users").child(uid).child("EXAMS")
        database.child(exam).setValue(newExam)
        Toast.makeText(this, newExam.ex.toString() + " Added to database", Toast.LENGTH_LONG).show()

    }

    private fun OnCLickSpecs(position: Int, lv: ListView) { // per le specifiche al click dell'elemento
        val item = lv.getItemAtPosition(position) as Exam
        val d = Dialog(this)

        d.setContentView(R.layout.click_dialog)
        val titleDialog: TextView?=d.findViewById(R.id.titleDialog)
        val infoDialog:TextView?=d.findViewById(R.id.infoDialog)
        val dateDialog:TextView?=d.findViewById(R.id.dateDialog)
        titleDialog?.text=item.ex
        infoDialog?.text= item.sub+" - "+item.typ
        dateDialog?.text= item.dateAndHour
        d.show()

    }

    private fun onLongClickRemoveItem(position: Int, lv:ListView):Boolean {// per cancellare con il long click

        val item = lv.getItemAtPosition(position) as Exam
        AlertDialog.Builder(this)
            .setTitle("Delete Exam")
            .setMessage("Are you sure you want to delete this exam?") // Specifying a listener allows you to take an action before dismissing the dialog.
            // The dialog is automatically dismissed when a dialog button is clicked.
            .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    Toast.makeText(
                        applicationContext, "Deleted ${item.ex}",
                        Toast.LENGTH_LONG
                    ).show()
                    val uid= auth.currentUser!!.uid
                    database = Firebase.database.reference.child("Users").child(uid).child("EXAMS")
                    database.child(item.ex).removeValue()

                    // delete the item from the listview data structure
                    exams.removeAt(position)
                    // notify the data set has changed to the listview adapter
                    adapter.notifyDataSetChanged()
                }
            }) // A null listener allows the button to dismiss the dialog and take no further action.
            .setNegativeButton(android.R.string.no, null)
            .setIcon(drawable.ic_alert_foreground)
            .show()

        return true
    }


}
